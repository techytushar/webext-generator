  document.getElementById('page2').style.display = 'none';

  // Assigning default Values for the Variables
  var extname = "Example Extension", extversion = "1.0", extdescription = "Extension Description", extpos = "";

  //Creation of required Objects.
  var data = new Object();

  document.getElementById("next").addEventListener("click", function(){

    var dummy = new Object();

    // Assigning user entered for the Variables
    var dummy = document.getElementById('extname').value;
    if(dummy != "") {
       extname = dummy;
    }
    console.log("Name : " + extname);

    var dummy = document.getElementById('extdescription').value;
     if(dummy != "") {
      extdescription = dummy;
    }
    console.log("Description : " + extdescription);

    var dummy = document.getElementById('extversion').value;
     if(dummy != "") {
       extversion = dummy;
    }
    console.log("Version : " + extversion);

    //Assigning values to Objects.
    data.name = extname;
    data.description = extdescription;
    data.version = extversion;

    var dummy2 = new Object();

    if(document.getElementById("page").checked) {
        console.log("Entered the Page Action sequence");

        dummy2.default_icon = {
          "32":"path/example-16X16.png",
          "64":"path/example-32X32.png"
        };

        dummy2.default_title = "Title";
        dummy2.default_popup = "path/examplepage.html";

        data.page_action = dummy2;

        console.log(data.page_action);

        document.getElementById('page1').style.display = 'none';
        document.getElementById('page2').style.display = 'block';


    } else if(document.getElementById("browser").checked) {
      console.log("Entered the Browser Action sequence");

      dummy2.default_icon = {
        "32":"path/example-16X16.png",
        "64":"path/example-32X32.png"
      };
      dummy2.default_title = "Title";
      dummy2.default_popup = "path/examplepage.html";

      data.browser_action = dummy2;

      console.log(data.page_action);

      document.getElementById('page1').style.display = 'none';
      document.getElementById('page2').style.display = 'initial';

    }

  }, false);

  document.getElementById("submit").addEventListener("click", function(){

    var checkboxes = document.getElementsByName('check');
    var checkboxesChecked = [];

    // loop over them all
    for (var i=0; i < checkboxes.length; i++) {
       // And stick the checked ones onto an array...
       if (checkboxes[i].checked) {
          checkboxesChecked.push(checkboxes[i].id);
       }
    }

    if(checkboxesChecked.length > 0) {
      data.permissions = checkboxesChecked;
    }


    //Creating the object text
    var datatext = JSON.stringify(data, null, 6);
    console.log(datatext);

    var filename = "manifest.json";

    // Generate download with JSON content
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(datatext));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);


  }, false);

  document.getElementById("back").addEventListener("click", function(){
    document.getElementById('page1').style.display = 'block';
    document.getElementById('page2').style.display = 'none';
  }, false);

console.log("End of the Program");
